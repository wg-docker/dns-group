FROM alpine:latest

RUN apk add --update-cache \
    supervisor \
    dnsmasq \
    dnscrypt-proxy \
  && rm -rf /var/cache/apk/*

EXPOSE 53/udp
EXPOSE 53/tcp
  
CMD ["/usr/bin/supervisord", "-c", "/app/supervisord.conf"]